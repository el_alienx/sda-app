// Core
import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "utils/fontawesome";

export default function ItemNavigation({ icon, label }) {
  return (
    <React.Fragment>
      <div className="icon">
        <FontAwesomeIcon icon={icon} />
      </div>
      <div className="label">{label}</div>
    </React.Fragment>
  );
}
