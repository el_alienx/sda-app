import { eDashboardType } from "./eDashboardType";

export default interface iDashboardItem {
  id: number;
  name: string;
  fileName: string;
  date: string;
  type: eDashboardType;
}
