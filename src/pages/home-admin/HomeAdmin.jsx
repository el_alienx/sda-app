// Core
import { useEffect } from "react";

// Internal
import Page from "components/Page";
import Card from "components/Card";
import PictureAdmin from "assets/images/illustrations/course-list.png";
import "./HomeAdmin.sass";

export default function HomeAdmin() {
  // Properties
  const adminItems = [
    {
      id: 1,
      name: "Module contents",
      image: PictureAdmin,
      date: "",
      route: "course-list",
      type: "admin",
    },
  ];

  // Methods
  useEffect(() => {
    document.title = `SDA Platform`;
  }, []);

  // Components
  const Cards = ({ list }) => {
    return list.map((item) => {
      return <Card key={item.id} props={item} route={item.route} />;
    });
  };

  return (
    <Page id="dashboard-admin">
      <h1>
        SDA Admin <br />
        Platform
      </h1>

      <h2>Admin task</h2>
      <div className="grid-container">
        <Cards list={adminItems} />
      </div>
    </Page>
  );
}
