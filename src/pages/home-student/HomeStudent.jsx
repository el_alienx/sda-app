// Core
import React, { useEffect } from "react";
import { useRecoilValue } from "recoil";

// Internal
import { coursesState } from "state/courseState";
import Card from "components/Card";
import Page from "components/Page";
import "./HomeStudent.sass";

export default function HomeStudent() {
  // Properties
  const courses = useRecoilValue(coursesState);

  // Methods
  useEffect(() => {
    document.title = "SDA Platform";
  }, []);

  // Components
  const FilteredModules = ({ type }) => {
    const list = courses.filter((item) => item.type === type);

    return list.map((item) => {
      return <Card key={item.id} props={item} route={`course/${item.id}`} />;
    });
  };

  return (
    <Page id="dashboard-student">
      <h1>Home</h1>

      <h2>Learning Modules</h2>
      <div className="grid-container">
        <FilteredModules type="learning" />
      </div>

      <h2>Evaluation Modules</h2>
      <div className="grid-container">
        <FilteredModules type="evaluation" />
      </div>
    </Page>
  );
}
