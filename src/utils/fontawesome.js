// Credit: https://www.digitalocean.com/community/tutorials/how-to-use-font-awesome-5-with-react
// Clean up imports by using: https://sortmylist.com

// Core
import { library } from "@fortawesome/fontawesome-svg-core";

// Icons
import {
  faBookOpen,
  faCalendarAlt,
  faChalkboardTeacher,
  faCode,
  faCommentAlt,
  faExclamationCircle,
  faFileAlt,
  faFileCode,
  faFolder,
  faImage,
  faLayerGroup,
  faListUl,
  faPencilAlt,
  faPlay,
  faUser,
  faInfo,
  faGlobe,
  faProjectDiagram,
  faLaptopCode,
  faTrophy,
  faLock,
  faChartLine,
  faCheckSquare,
  faVideo,
  faPlus,
  faTrashAlt,
  faCheck,
  faChevronDown,
} from "@fortawesome/free-solid-svg-icons";

import { faGoogleDrive, faSlack } from "@fortawesome/free-brands-svg-icons";

library.add(
  faBookOpen,
  faCalendarAlt,
  faChalkboardTeacher,
  faCode,
  faCommentAlt,
  faExclamationCircle,
  faFileAlt,
  faFileCode,
  faFolder,
  faImage,
  faLayerGroup,
  faListUl,
  faPencilAlt,
  faPlay,
  faUser,
  faInfo,
  faGlobe,
  faProjectDiagram,
  faLaptopCode,
  faTrophy,
  faLock,
  faChartLine,
  faCheckSquare,
  faVideo,
  faPlus,
  faTrashAlt,
  faCheck,
  faChevronDown,
  faSlack,
  faGoogleDrive
);
