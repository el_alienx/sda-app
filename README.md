# SDA Platform

The platform for both students and admins of the Software Development Academy.

## Installation

1. Clone the project
2. Run the installation `npm install`
3. Start the project `npm start`
